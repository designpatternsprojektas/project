﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    public partial class AboutForm : Form {
        public AboutForm() {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void Close(object sender, EventArgs e) {
            this.Hide();
        }
    }
}
