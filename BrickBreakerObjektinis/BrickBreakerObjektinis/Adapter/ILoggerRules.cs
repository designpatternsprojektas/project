﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.Adapter
{
    interface ILoggerRules
    {
        void Print(string tag, string msg);
    }
}
