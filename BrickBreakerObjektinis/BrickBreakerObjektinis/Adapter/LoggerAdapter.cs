﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.Adapter
{
    class LoggerAdapter : ILoggerRules
    {
        private Logger logger;

        public LoggerAdapter(Logger l)
        {
            logger = l;
        }

        public void Print(string tag, string msg)
        {
            logger.PrintToLog(tag, msg);
            logger.PrintToConsole(tag, msg);
        }
    }
}
