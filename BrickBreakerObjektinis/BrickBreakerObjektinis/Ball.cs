﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    class Ball : IBall {
        public PictureBox ball;
        public double speed_left = 10;
        public double speed_top = 10;
        public bool isMain;

        public Ball(PictureBox ball, bool type) {
            this.ball = ball;
            this.ball.Image = TextureHandler.GetInstance().GetImage("ball.png");

            isMain = type;

            this.ball.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        public IBall MakeCopy(PictureBox picBox) {
            Ball b = (Ball) MemberwiseClone();
            b.ball = picBox;
            b.isMain = false;
            b.ball.Image = this.ball.Image;
            b.ball.SizeMode = this.ball.SizeMode;
            b.speed_left = this.speed_left;
            b.speed_top = this.speed_top;

            return (IBall) b;
        }

    }
}
