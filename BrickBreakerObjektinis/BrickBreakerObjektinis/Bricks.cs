﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using BrickBreakerObjektinis.BuildingBricks;
using BrickBreakerObjektinis.BuildingBricks.brick_types;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;
using BrickBreakerObjektinis.flyweight;
using Microsoft.VisualBasic.PowerPacks;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis {

    class Bricks {
        public RectangleShape[] vectRectShape = new RectangleShape[100];
        public int[] colorType = new int[100];
        public Rectangle[] vectRectangles = new Rectangle[100];
        public Dictionary<int, PowerUp> SpecialDictionary = new Dictionary<int, PowerUp>();
        public Dictionary<int, int> BbricksCollisionList = new Dictionary<int, int>();
        public int j = 0;
        private int w, h;

        

        public Bricks(int w, int h) {
            this.w = w;
            this.h = h;

            Random rnd = new Random();
            int widthRect = Convert.ToInt32(w / 10); // bloko ilgis
            for (int row = h / 30; row < h / 3; row += h / 20) {
                for (double platform = (w / 100); platform < w; platform += (w / 9))  {
                    int predictable = Convert.ToInt32(platform) + (w / 10); // kitas blokas
                    if (predictable < w) {
                        int x = Convert.ToInt32(platform);
                        int y = 70 + row;
                       
                        int randomBrick = rnd.Next(1, 4);
                        int randomColor = rnd.Next(1, 4);

                        AbstractFactory brickFactory = FactoryProducer.GetFactory(1);
                        AbstractFactory colorFactory = FactoryProducer.GetFactory(2);

                        IBrick brick = brickFactory.GetBrick(randomBrick);
                        IColor color = colorFactory.GetColor(randomColor);

                        
                        PowerUp_Factory pUp_Factory = new PowerUp_Factory();
                        PictureBox picture = new PictureBox {
                            Name = "pictureBox" + x,
                            Size = new Size(316, 320),
                            Location = new Point(x + widthRect / 2 - 15, y),
                            BorderStyle = BorderStyle.FixedSingle,
                            SizeMode = PictureBoxSizeMode.Zoom
                        };
            
                        SpecialDictionary.Add(j, pUp_Factory.CreatePowerUp(x + widthRect / 2 - 15, y, picture));
                        BbricksCollisionList.Add(j, brick.BrickStrength());
                        vectRectShape[j] = new RectangleShape(x, y, widthRect, 30);
                        colorType[j] = randomColor;

                        Color gotColor = color.Color();
                        Bitmap bmp = BmpFactory.GetBitmap(gotColor);
                        
                        vectRectShape[j].BackgroundImage = bmp;
                    }
                    j++;
                }
            }
        }

        public void Paint() {
            int i = 0;
            for (int row = h / 30; row < h / 3; row += h / 20) {
                for (double plataform = (w / 100); plataform < w; plataform += w / 9) {

                    int widthRect = Convert.ToInt32(w / 10);
                    int predict = Convert.ToInt32(plataform) + (w / 10);

                    if (predict < w) {
                        vectRectangles[i] = new Rectangle(Convert.ToInt32(plataform), 70 + row, widthRect, 30);
                    }
                    i++;
                }
            }
        }



    }
}
