﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BricksBuilding {
    abstract class AbstractBrick {
        public int CollisionTimes { get; set; }
        public string Img { get; set; }
        public int ItemId { get; set; }

        public abstract void MakeBrick();
    }
}
