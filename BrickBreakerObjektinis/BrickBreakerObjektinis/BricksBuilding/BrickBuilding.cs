﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BricksBuilding {
    abstract class BrickBuilding {
        protected abstract AbstractBrick MakeBrick(int typeOf);

        public AbstractBrick OrderBrick(int typeOf) {
            AbstractBrick brick = MakeBrick(typeOf);
            brick.MakeBrick();

            return brick;
        }
    }
}
