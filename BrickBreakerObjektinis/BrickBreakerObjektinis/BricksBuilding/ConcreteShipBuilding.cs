﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BricksBuilding {
    class ConcreteShipBuilding : BrickBuilding  {
        protected override AbstractBrick MakeBrick(int typeOf) {
            AbstractBrick brick = null;
            switch (typeOf) {
                case 1:
                    IBrickBuilderFactory b1 = new CasualBrickFactory();
                    brick = new EmptyBrick(b1);
                    break;
                case 2:

                    break;
            }
            return brick;
        }
    }
}