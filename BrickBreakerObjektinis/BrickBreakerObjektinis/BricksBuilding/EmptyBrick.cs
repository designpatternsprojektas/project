﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BricksBuilding {
    class EmptyBrick : AbstractBrick {
        private readonly IBrickBuilderFactory _brickBuilderFactory;

        public override void MakeBrick() {
            CollisionTimes = _brickBuilderFactory.AddCollisionTimes();
            Img = _brickBuilderFactory.AddImgPath();
            ItemId = _brickBuilderFactory.AddItemId();
        }

        public EmptyBrick(IBrickBuilderFactory brickFactory) {
            _brickBuilderFactory = brickFactory;
        }
    }
}
