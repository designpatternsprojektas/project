﻿namespace BrickBreakerObjektinis.BricksBuilding {
    public interface IBrickBuilderFactory {
        int AddCollisionTimes();
        string AddImgPath();
        int AddItemId();
    }
}