﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks
{
    abstract class AbstractFactory
    {
        public abstract IBrick GetBrick(int color);
        public abstract IColor GetColor(int shape);
    }
}
