﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.Adapter;
using BrickBreakerObjektinis.BuildingBricks.brick_types;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks
{
    class BrickFactory : AbstractFactory
    {
        LoggerAdapter adapter = new LoggerAdapter(Logger.GetInstance());
        public override IBrick GetBrick(int color)
        {
            switch (color)
            {
                case 1:
                    adapter.Print("BrickFactoryTag", "new WeakBrick()");
                    return new WeakBrick();
                case 2:
                    adapter.Print("BrickFactoryTag", "new MediumBrick()");
                    return new MediumBrick();
                case 3:
                    adapter.Print("BrickFactoryTag", "new StrongBrick()");
                    return new StrongBrick();
            }
            return null;
        }

        public override IColor GetColor(int shape)
        {
            return null;
        }
    }
}
