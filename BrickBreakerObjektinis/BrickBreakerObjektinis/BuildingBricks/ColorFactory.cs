﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.BuildingBricks.brick_colors;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks
{
    class ColorFactory : AbstractFactory
    {
        public override IBrick GetBrick(int color)
        {
            return null;
        }

        public override IColor GetColor(int shape)
        {
            switch (shape)
            {
                case 1:
                    return new Red();
                case 2:
                    return new Green();
                case 3:
                    return new Blue();
            }
            return null;
        }
    }
}
