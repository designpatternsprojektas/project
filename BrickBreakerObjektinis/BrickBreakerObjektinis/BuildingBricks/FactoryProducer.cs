﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BuildingBricks
{
    class FactoryProducer
    {
        public static AbstractFactory GetFactory(int factory)
        {
            switch (factory)
            {
                case 1:
                    return new BrickFactory();
                case 2:
                    return new ColorFactory();
            }
            return null;
        }
    }
}
