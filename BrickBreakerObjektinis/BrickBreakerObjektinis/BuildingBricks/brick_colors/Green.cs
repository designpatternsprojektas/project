﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks.brick_colors
{
    class Green : IColor
    {
        public Color Color()
        {
            return GameOptionsSingleton.GetInstance().GetBrickColor2();
        }
    }
}
