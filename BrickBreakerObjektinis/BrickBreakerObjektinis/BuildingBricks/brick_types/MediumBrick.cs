﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks.brick_types
{
    class MediumBrick : IBrick
    {
        public int BrickStrength()
        {
            return 2;
        }

        public string BrickTexture()
        {
            return "brick_medium.png";
        }

        public int BrickItems()
        {
            return 1;
        }
    }
}
