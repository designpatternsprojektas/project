﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks.brick_types
{
    class StrongBrick : IBrick
    {
        public int BrickStrength()
        {
            return 3;
        }

        public string BrickTexture()
        {
            return "brick_special.png";
        }

        public int BrickItems()
        {
            return -1;
        }
    }
}
