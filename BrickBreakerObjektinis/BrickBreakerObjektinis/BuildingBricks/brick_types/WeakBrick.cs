﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.BuildingBricks.builder_interfaces;

namespace BrickBreakerObjektinis.BuildingBricks.brick_types
{
    class WeakBrick : IBrick
    {
        public int BrickStrength()
        {
            return 1;
        }

        public string BrickTexture()
        {
            return "brick.png";
        }

        public int BrickItems()
        {
            return -1;
        }
    }
}
