﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BuildingBricks.builder_interfaces
{
    interface IBrick
    {
        int BrickStrength();
        String BrickTexture();
        int BrickItems();
    }
}
