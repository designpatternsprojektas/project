﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.BuildingBricks.builder_interfaces
{
    interface IColor
    {
        Color Color();
    }
}
