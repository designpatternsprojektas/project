﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.CommandPattern {
    class BrickColor3DownCommand : Command {

        public void Execute() {
            GameOptionsSingleton.GetInstance().DecColor(3);
        }

        public void Undo() {
            GameOptionsSingleton.GetInstance().IncColor(3);
        }
    }
}
