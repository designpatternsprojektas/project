﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.CommandPattern {
    class BrickColor3UpCommand : Command {

        public void Execute() {
            GameOptionsSingleton.GetInstance().IncColor(3);
        }

        public void Undo() {
            GameOptionsSingleton.GetInstance().DecColor(3);
        }
    }
}
