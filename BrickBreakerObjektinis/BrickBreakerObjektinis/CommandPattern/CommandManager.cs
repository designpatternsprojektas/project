﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.CommandPattern {
    class CommandManager {

        private Stack<Command> commandStack = new Stack<Command>();

        public void ExecuteCommand(Command cmd) {
            commandStack.Push(cmd);
            cmd.Execute();
        }

        public void UndoCommand() {
            if(commandStack.Count > 0) {
                Command cmd = commandStack.Pop();
                cmd.Undo();
            }
                
        }
    }
}
