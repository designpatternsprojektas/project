﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BrickBreakerObjektinis {
    class GameOptionsSingleton {

        private static GameOptionsSingleton optionsInstance;

        private Dictionary<int, Color> colorDict = new Dictionary<int, Color>();

        private int soundSetting;
        private int brickColor1;
        private int brickColor2;
        private int brickColor3;

        private GameOptionsSingleton() {
            AddToColorDict();
            soundSetting = 3;
            brickColor1 = 3;
            brickColor2 = 2;
            brickColor3 = 1;
        }

        public static GameOptionsSingleton GetInstance() {
            if (optionsInstance == null) {
                optionsInstance = new GameOptionsSingleton();
            }
            return optionsInstance;
        }

        public int GetSoundSetting() {
            return soundSetting;
        }

        public Color GetBrickColor1() { return colorDict[brickColor1]; }
        public Color GetBrickColor2() { return colorDict[brickColor2]; }
        public Color GetBrickColor3() { return colorDict[brickColor3]; }

        public int GetBrickColor1Int() { return brickColor1; }
        public int GetBrickColor2Int() { return brickColor2; }
        public int GetBrickColor3Int() { return brickColor3; }

        public void SetAll(int sound, int bC1, int bC2, int bC3) {
            this.soundSetting = sound;
            this.brickColor1 = bC1;
            this.brickColor2 = bC2;
            this.brickColor3 = bC3;
        }


        public void IncSound() {
            if (soundSetting < 10) {
                Logger.GetInstance().PrintToLog("GameSettings", "Sound Increased. Created at: " + DateTime.Now);
                soundSetting++;
            }
        }

        public void DecSound() {
            if (soundSetting > 0) {
                Logger.GetInstance().PrintToLog("GameSettings", "Sound Decreased. Created at: " + DateTime.Now);
                soundSetting--;
            }
        }

        public void IncColor(int brick) {

            switch (brick) {
                case 1:
                    if (brickColor1 < 10)
                        brickColor1++;
                    break;
                case 2:
                    if (brickColor2 < 10)
                        brickColor2++;
                    break;
                case 3:
                    if (brickColor3 < 10)
                        brickColor3++;
                    break;
                default:
                    Console.WriteLine("Bad block pick, in IncColor, Game Options");
                    break;
            }

        }

        public void DecColor(int brick) {

            switch (brick) {
                case 1:
                    if (brickColor1 > 1)
                        brickColor1--;
                    break;
                case 2:
                    if (brickColor2 > 1)
                        brickColor2--;
                    break;
                case 3:
                    if (brickColor3 > 1)
                        brickColor3--;
                    break;
                default:
                    Console.WriteLine("Bad block pick, in DecColor, Game Options");
                    break;
            }
        }

        private void AddToColorDict() {
            colorDict.Add(1, Color.Red);
            colorDict.Add(2, Color.Green);
            colorDict.Add(3, Color.Blue);
            colorDict.Add(4, Color.Yellow);
            colorDict.Add(5, Color.Orange);
            colorDict.Add(6, Color.Black);
            colorDict.Add(7, Color.Cyan);
            colorDict.Add(8, Color.Gray);
            colorDict.Add(9, Color.Navy);
            colorDict.Add(10, Color.Silver);
        }
    }
}
