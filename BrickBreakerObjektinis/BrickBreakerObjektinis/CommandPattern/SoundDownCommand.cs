﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.CommandPattern {
    class SoundDownCommand : Command {
        public void Execute() {
            GameOptionsSingleton.GetInstance().DecSound();
        }

        public void Undo() {
            GameOptionsSingleton.GetInstance().IncSound();
        }
    }
}
