﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.CommandPattern {
    class SoundUpCommand : Command {
        public void Execute() {
            GameOptionsSingleton.GetInstance().IncSound();
        }

        public void Undo() {
            GameOptionsSingleton.GetInstance().DecSound();
        }
    }
}
