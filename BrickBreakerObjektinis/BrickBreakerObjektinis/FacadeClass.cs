﻿using BrickBreakerObjektinis.LogIterator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis {

    class FacadeClass {

        Form1 playFrom;
        AboutForm about;
        OptionsForm options;
        LogWindow log;

        public FacadeClass() {
            about = new AboutForm();
            options = new OptionsForm();
        }

        public void StartGame() {
            playFrom = new Form1();
            playFrom.Show();
            playFrom.SetPlay(true);
        }

        public void ShowOptions() {
            options.Show();
        }

        public void ShowAbout() {
            about.Show();
        }

        public void ShowLog() {
            log = new LogWindow();
            log.Show();
        }
    }

}
