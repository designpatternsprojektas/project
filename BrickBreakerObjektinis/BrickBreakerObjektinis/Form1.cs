﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.bricks_observers;
using BrickBreakerObjektinis.chain_of_responsibility;
using BrickBreakerObjektinis.color_counter.subject;
using Microsoft.VisualBasic.PowerPacks;
using BrickBreakerObjektinis.PowerUps;
using System;
using System.Threading;


namespace BrickBreakerObjektinis {

    public partial class Form1 : Form, ISubject {
        private ShapeContainer shapeContainer1;
        private ShapeContainer shapeContainer2;
        //private Score score;
        private Player player;
        private Bricks bricks;
        private Ball theBall;
        private Ball theSecondaryBall;
        private static List<PowerUp> powerUpItems;
        private List<IObserver> observers = new List<IObserver>();
        private int red = 0;
        private int green = 0;
        private int blue = 0;
        private int totalPoints = 0;

        private bool play = false;
        private static int FallingObjectId = 0;

        

       

        public Form1() {
            InitializeComponent();
            this.shapeContainer1 = new ShapeContainer();
 
            timer1.Enabled = true;
           // Cursor.Hide();

           // this.FormBorderStyle = FormBorderStyle.None;
           // this.TopMost = true;
            this.Bounds = Screen.PrimaryScreen.Bounds;

            //score = new Score();
            powerUpItems = new List<PowerUp>();
 

            racket.Top = playground.Bottom - (playground.Bottom / 10);
           // shapes.Add(FallObjectFactory.GetShape(picture));
            BlueBrickObserver blueBrickObserver = new BlueBrickObserver(this, blueLabel);
            RedBrickObserver redBrickObserver = new RedBrickObserver(this, redLabel);
            GreenBrickObserver reBrickObserver = new GreenBrickObserver(this, greenLabel);

            theBall = new Ball(ball, true);

            theSecondaryBall = null;

            player = new Player(ball, racket);
            bricks = new Bricks(playground.Width, playground.Height);

      

            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer2";

            for (int k = bricks.j; k > 0; k--) {
                if (bricks.vectRectShape[k - 1] != null) {
                    this.shapeContainer1.Shapes.Add(bricks.vectRectShape[k - 1]);
                } 
            }




            this.shapeContainer1.Size = new System.Drawing.Size(663, 329);
            this.shapeContainer1.TabIndex = 13;
            this.shapeContainer1.TabStop = false;

            bool add = true;
            foreach (KeyValuePair<int, PowerUp> entry in bricks.SpecialDictionary) {
                if (add)
                {
                    this.shapeContainer1.Shapes.Add(entry.Value.rectangleShape);
                }

                add = !add;

            }

           

            //shapeContainer2.Shapes.Add(f.rectangleShape);


            //this.playground.Controls.Add(this.picture);
            this.playground.Controls.Add(this.shapeContainer1);
            this.playground.Controls.Add(this.player.racket);
            this.playground.Controls.Add(this.theBall.ball);


        }

        private void Form1_Load(object sender, EventArgs e) {

        }



        private void timer1_Tick(object sender, EventArgs e) {
            if (play) {
                player.racket.Left = Cursor.Position.X - (player.racket.Width / 2);
                player.moverPlatform.X = Cursor.Position.X - (player.racket.Width / 2);

                player.rectPlayer.Location = theBall.ball.Location;

                theBall.ball.Left += Convert.ToInt32(theBall.speed_left);
                theBall.ball.Top += Convert.ToInt32(theBall.speed_top);

                foreach (PowerUp item in powerUpItems) {
                    item.DropMe();
                }

          

            

                PlatformCollisionCheck(theBall);
                ScreenBoundsCheck(theBall);
                CollisionCheck(theBall);
                PowerUpCollisionCheck();

                if (theSecondaryBall != null) {
                    player.rectPlayer.Location = theSecondaryBall.ball.Location;

                    ScreenBoundsCheck(theSecondaryBall);
                    PlatformCollisionCheck(theSecondaryBall);
                    CollisionCheck(theSecondaryBall);
                    if (theSecondaryBall != null) {
                        theSecondaryBall.ball.Left += Convert.ToInt32(theSecondaryBall.speed_left);
                        theSecondaryBall.ball.Top += Convert.ToInt32(theSecondaryBall.speed_top);

                    }
                }
            }
        }

        public void SetPlay(bool b) {
            play = b;
        }

        private void ShowGameOver() {
            this.Close();
            GameOverForm form = new GameOverForm();
            form.Show();
        }

        private void playground_Paint(object sender, PaintEventArgs e) {
            bricks.Paint();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            switch (e.KeyCode)
            {
                case Keys.Space:
                    AbstractDestroyer loggerChain = GetChainOfDestroyers();
                    loggerChain.DestroyItem(AbstractDestroyer.Blue);
                    break;
            }
        }

        private static AbstractDestroyer GetChainOfDestroyers()
        {
            AbstractDestroyer.powerUpItems = powerUpItems;
            AbstractDestroyer speedIncDestroyer = new SpeedIncDestroyer(AbstractDestroyer.Blue);
            AbstractDestroyer speedDecDestroyer = new SpeedDecDestroyer(AbstractDestroyer.Red);
            AbstractDestroyer pointIncDestroyer = new PointIncDestroyer(AbstractDestroyer.Green);
            AbstractDestroyer pointDecDestroyer = new PointDecDestroyer(AbstractDestroyer.Black);

            speedIncDestroyer.SetNextDestroyer(speedDecDestroyer);
            speedDecDestroyer.SetNextDestroyer(pointIncDestroyer);
            pointIncDestroyer.SetNextDestroyer(pointDecDestroyer);

            return speedIncDestroyer;
        }
        private void PlatformCollisionCheck(Ball checkedBall) { 
            if(checkedBall != null)
            if (checkedBall.ball.Bottom >= player.racket.Top && checkedBall.ball.Bottom <= player.racket.Bottom && checkedBall.ball.Left >= player.racket.Left && checkedBall.ball.Right <= player.racket.Right) {
                //theBall.speed_top += 0.5;
                //theBall.speed_left += 0.5;
                checkedBall.speed_top = -checkedBall.speed_top;
            }
            
        }

        private void ScreenBoundsCheck(Ball checkedBall) {
            if (checkedBall.ball.Left <= playground.Left) {
                checkedBall.speed_left = -theBall.speed_left;
            }

            if (checkedBall.ball.Right >= playground.Right) {
                checkedBall.speed_left = -theBall.speed_left;
            }

            if (checkedBall.ball.Top <= playground.Top) {
                checkedBall.speed_top = -theBall.speed_top;
            }

            if (checkedBall.ball.Bottom >= playground.Bottom && checkedBall.isMain) {
                timer1.Enabled = false;
                ShowGameOver();
            }
            else if (checkedBall.ball.Bottom >= playground.Bottom && !checkedBall.isMain) {
                this.playground.Controls.Remove(theSecondaryBall.ball);
                theSecondaryBall = null;
            }
        }

        private void CollorCollisionCounter(int color)
        {
            switch (color)
            {
                case 1:
                    red++;
                    break;
                case 2:
                    green++;
                    break;
                case 3:
                    blue++;
                    break;
            }
        }

        private void CollisionCheck(Ball checkedBall) {
     
            for (int l = bricks.j; l > 0; l--) {
                int index = l - 1;
                if (bricks.vectRectShape[index].Visible && 
                    bricks.vectRectangles[index].IntersectsWith(player.rectPlayer)) {
                     bricks.vectRectShape[index].Visible = false;
                     CollorCollisionCounter(bricks.colorType[index]);

                    NotifyAll(red, green, blue);

                    checkedBall.speed_top = -checkedBall.speed_top;

                    if (bricks.BbricksCollisionList.ContainsKey(index))
                    {
                        int times;
                        bricks.BbricksCollisionList.TryGetValue(index, out times);
                        times--;
                        bricks.BbricksCollisionList.Remove(index);
                        bricks.BbricksCollisionList.Add(index, times);
                        if (times <= 0)
                        {
                            bricks.BbricksCollisionList.Remove(index);
                            bricks.vectRectShape[index].Visible = false;
                        }
                    }

                    if (bricks.SpecialDictionary.ContainsKey(index)) {
                        PowerUp tmp;
                        bool hasValue = bricks.SpecialDictionary.TryGetValue(index, out tmp);
                        if (hasValue) {
                            powerUpItems.Add(tmp);
                        }
                        bricks.SpecialDictionary.Remove(index);
                    }
                }
            }
        }

        private void PowerUpCollisionCheck() {
            List<PowerUp> toDelete = new List<PowerUp>();
            PictureBox pb = new PictureBox() {
                Size = new Size(40, 37),
                Location = new Point(533, 492),
            };
            for (int i=0; i<powerUpItems.Count; i++)
            {
                PowerUp pUp = powerUpItems[i];
                if (pUp.rectangleShape.Visible && pUp.rectangle.IntersectsWith(player.moverPlatform))
                {
                    if (pUp.GetPowerUp(totalPoints) != 0) {
                        totalPoints = pUp.GetPowerUp(totalPoints);
                        NotifyAll(red, green, blue);
                    } else if (pUp.GetPowerUp(totalPoints) != 0) {
                        theBall.speed_left = pUp.GetPowerUp(totalPoints);
                        theBall.speed_top = pUp.GetPowerUp(totalPoints);
                        if (theSecondaryBall != null) {
                            theSecondaryBall.speed_left = pUp.GetPowerUp(totalPoints);
                            theSecondaryBall.speed_top = pUp.GetPowerUp(totalPoints);
                        }
                    } else {
                        if (theSecondaryBall == null) {
                            theSecondaryBall = (Ball) theBall.MakeCopy(pb);
                            theSecondaryBall.speed_left = 5;
                            theSecondaryBall.speed_top = 5;
                            this.playground.Controls.Add(theSecondaryBall.ball);
                        }
                    }
                    pUp.rectangleShape.Visible = false;
                    toDelete.Add(pUp);
                }

            }

            foreach (PowerUp toDel in toDelete)
            {
                powerUpItems.Remove(toDel);
            }
            toDelete.Clear();
        }

        public static void DeleteSpecificItem(int code)
        {
              List<PowerUp> toDelete = new List<PowerUp>();

            foreach (PowerUp toDel in powerUpItems)
            {
                toDelete.Add(toDel);
                toDel.rectangleShape.Visible = false;
            }

            foreach (PowerUp toDel in toDelete)
            {
                powerUpItems.Remove(toDel);
            }



            toDelete.Clear();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void Register(IObserver o)
        {
            observers.Add(o);
        }

        public void Unregister(IObserver o)
        {
            int index = observers.IndexOf(o);
            observers.RemoveAt(index);
        }

        public void NotifyAll(int red, int green, int blue)
        {
            totalPointsLabel.Text = "Taškai: " + ++totalPoints;
            foreach (IObserver observer in observers)
            {
                observer.update(red, green, blue);
            }
        }
    }
}