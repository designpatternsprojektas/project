﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.LogIterator {
    class Aggregate : AggregateInterface {

        private ArrayList items = new ArrayList();
        String logFile = "Log.txt";

        public Aggregate() { }

        public int Count() {
            return items.Count;
        }

        public object this[int index] {
            get { return items[index]; }
        }

        public Iterator CreateIterator() {
            return new Iterator(this);
        }

        public void Remove(int index) {
            items.RemoveAt(index);
        }

        public void GetItemsFromLogFile() {

            if (File.Exists(logFile)) {
                var lines = File.ReadLines(logFile);
                foreach (string line in lines) {
                    string tag = line.Substring(0, line.IndexOf(' '));
                    string text = line.Substring(line.IndexOf(' '));
                    items.Add(new LogItem(tag, text));
                }
            }

            
        }


    }
}
