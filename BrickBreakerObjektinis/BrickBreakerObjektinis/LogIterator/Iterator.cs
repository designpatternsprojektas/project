﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.LogIterator {
    class Iterator : IteratorInterface {

        private Aggregate aggregation;
        private int current = 0;

        public Iterator(Aggregate agr) {
            this.aggregation = agr;
        }

        public LogItem First() {
            current = 0;
            if(isDone()) {
                return null;
            }
            return aggregation[current] as LogItem;
        }

        public bool isDone() {
            return current >= aggregation.Count();
        }

        public LogItem Prev() {
            current--;
            if (isDone()) {
                current = 0;
                return null;
            }
            else if (current < 0) {
                current = 0;
                return null;
            }
            else {
                return aggregation[current] as LogItem;

            }

        }

        public LogItem Next() {
            current++;
            if (current >= aggregation.Count()) current = aggregation.Count();
            if (isDone()) {
                return null;
            }
            else {
                return aggregation[current] as LogItem;
            }

        }

        public LogItem Remove() {
            if (isDone())
                return null;
            if(aggregation.Count() == 1) {
                aggregation.Remove(current);
                return null;
            }
            aggregation.Remove(current);
            current--;
            if (current < 0) current = 0;
            return aggregation[current] as LogItem;
        }

        public void PutChangesToFile() {
            this.current = 0;
            File.WriteAllText("Log.txt", string.Empty);

            while (current != aggregation.Count()) {
                Console.WriteLine(current + " " + aggregation.Count());

                    LogItem item = aggregation[current] as LogItem;
                    using (FileStream fs = new FileStream("Log.txt", FileMode.Append, FileAccess.Write))
                    using (StreamWriter sw = new StreamWriter(fs)) {
                        sw.WriteLine(item.GetItem());
                    }
                    current++;
                }
        }




    }
}
