﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.LogIterator {

    interface IteratorInterface {

        LogItem First();
        LogItem Next();
        LogItem Prev();
        bool isDone();
        LogItem Remove();
    }

}
