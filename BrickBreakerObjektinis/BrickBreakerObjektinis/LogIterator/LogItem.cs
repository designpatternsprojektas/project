﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.LogIterator {
    class LogItem {

        string tag;
        string text;

        public LogItem() {   }
        public LogItem(string t, string tt) {
            this.tag = t;
            this.text = tt;
        }


        public string GetItem() {
            return tag + text;
        }

    }
}
