﻿using BrickBreakerObjektinis.LogIterator;
using System;

using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    public partial class LogWindow : Form {

        Aggregate aggregation;
        Iterator iterator;
        LogItem item;

        public LogWindow() {
            aggregation = new Aggregate();
            iterator = new Iterator(aggregation);
            aggregation.GetItemsFromLogFile();

            item = new LogItem();
            InitializeComponent();
            this.CenterToScreen();

            item = iterator.First();
            Refresh();

        }

        private void First(object sender, EventArgs e) {
            item = iterator.First();
            Refresh();
        }

        private void Next(object sender, EventArgs e) {
            item = iterator.Next();
            Refresh();

        }

        private void Prev(object sender, EventArgs e) {
            item = iterator.Prev();
            Refresh();
        }

        private void Remove(object sender, EventArgs e) {
            item = iterator.Remove();
            Refresh();
        }

        private void Refresh() {
            if (item != null)
                this.richTextBox1.Text = item.GetItem();
            else
                this.richTextBox1.Text = "Nothing to show";
        }

        private void Close(object sender, EventArgs e) {
            iterator.PutChangesToFile();
            this.Close();
        }


    }
}
