﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis {
    class Logger
    {
        private static Logger LoggerInstance;

        String logFile = "Log.txt";

        private Logger()
        {
        }

        public static Logger GetInstance()
        {
            if (LoggerInstance == null)
            {
                LoggerInstance = new Logger();
            }
            return LoggerInstance;
        }

        public void PrintToLog(string tag, string msg)
        {
            if (!File.Exists(logFile))
            {
                File.Create(logFile);
            }

            using (FileStream fs = new FileStream(logFile, FileMode.Append, FileAccess.Write))
            using (StreamWriter sw = new StreamWriter(fs))
            {
                sw.WriteLine(tag + " " + msg);
            }
        }

        public void PrintToConsole(string tag, string msg)
        {
            Console.WriteLine(tag + ", " + msg);
        }
    }
}
