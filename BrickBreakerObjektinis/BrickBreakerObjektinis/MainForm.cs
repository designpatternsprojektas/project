﻿using BrickBreakerObjektinis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    public partial class MainForm : Form {

        private FacadeClass facade;

        public MainForm() {
            facade = new FacadeClass();
            InitializeComponent();
            this.CenterToScreen();

        }

        private void StartGame(object sender, EventArgs e) {
            facade.StartGame();
        }

        private void OptionsWindow(object sender, EventArgs e) {
            facade.ShowOptions();
        }

        private void AboutWindow(object sender, EventArgs e) {
            facade.ShowAbout();
        }

        private void LogWindow(object sender, EventArgs e) {
            facade.ShowLog();
        }

        private void CloseWindow(object sender, EventArgs e) {
            this.Close();
        }

        
    }
}
