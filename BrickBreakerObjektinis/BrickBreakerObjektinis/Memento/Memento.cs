﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.Memento {
    class Memento {

        private int soundSetting;
        private int brickColor1;
        private int brickColor2;
        private int brickColor3;

        public Memento(int sS, int bC1, int bC2, int bC3) {
            this.soundSetting = sS;
            this.brickColor1 = bC1;
            this.brickColor2 = bC2;
            this.brickColor3 = bC3;
        }

        public int SoundSetting {
            get { return soundSetting; }
            set { soundSetting = value; }
        }

        public int BrickColor1 {
            get { return brickColor1; }
            set { brickColor1 = value; }
        }

        public int BrickColor2 {
            get { return brickColor2; }
            set { brickColor2 = value; }
        }

        public int BrickColor3 {
            get { return brickColor3; }
            set { brickColor3 = value; }
        }

    }
}
