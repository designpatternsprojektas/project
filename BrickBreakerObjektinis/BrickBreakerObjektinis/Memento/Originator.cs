﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.Memento {
    class Originator {

        private int soundSetting;
        private int brickColor1;
        private int brickColor2;
        private int brickColor3;

        public int SoundSetting {
            get { return soundSetting; }
            set { soundSetting = value; }
        }

        public int BrickColor1 {
            get { return brickColor1; }
            set { brickColor1 = value; }
        }

        public int BrickColor2 {
            get { return brickColor2; }
            set { brickColor2 = value; }
        }

        public int BrickColor3 {
            get { return brickColor3; }
            set { brickColor3 = value; }
        }



        public Memento SaveMemento() {
            Logger.GetInstance().PrintToLog("Memento", "Created at: " + DateTime.Now);

            return new Memento(soundSetting, brickColor1, brickColor2, brickColor3);
        }

        public void LoadMemento(Memento memento) {
            this.soundSetting = memento.SoundSetting;
            this.brickColor1 = memento.BrickColor1;
            this.brickColor2 = memento.BrickColor2;
            this.brickColor3 = memento.BrickColor3;            
        }

    }
}
