﻿using BrickBreakerObjektinis.CommandPattern;
using BrickBreakerObjektinis.Memento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    public partial class OptionsForm : Form {

        private CommandManager commandManager;
        private Originator _originator;
        private Caretaker _caretaker;

        public OptionsForm() {
            commandManager = new CommandManager();
            InitializeComponent();

            _originator = new Originator();
            _caretaker = new Caretaker();

            RedrawPictureBoxes();
            RedrawSoundInt();
            this.CenterToScreen();
        }

        private void Brick1ColorInc(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new BrickColor3UpCommand());
            RedrawPictureBoxes();
        }

        private void Brick2ColorInc(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new BrickColor2UpCommand());
            RedrawPictureBoxes();
        }

        private void Brick3ColorInc(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new BrickColor1UpCommand());
            RedrawPictureBoxes();
        }

        private void Brick1ColorDec(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new BrickColor3DownCommand());
            RedrawPictureBoxes();
        }

        private void Brick2ColorDec(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new BrickColor2DownCommand());
            RedrawPictureBoxes();
        }

        private void Brick3ColorDec(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new BrickColor1DownCommand());
            RedrawPictureBoxes();
        }

        private void SoundInc(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new SoundUpCommand());
            RedrawSoundInt();
        }

        private void SoundDec(object sender, EventArgs e) {
            commandManager.ExecuteCommand(new SoundDownCommand());
            RedrawSoundInt();
        }

        private void Undo(object sender, EventArgs e) {
            commandManager.UndoCommand();
            RedrawPictureBoxes();
            RedrawSoundInt();
        }

        private void RedrawPictureBoxes() {
            this.pictureBox1.BackColor = GameOptionsSingleton.GetInstance().GetBrickColor3();
            this.pictureBox2.BackColor = GameOptionsSingleton.GetInstance().GetBrickColor2();
            this.pictureBox3.BackColor = GameOptionsSingleton.GetInstance().GetBrickColor1();

            if (GameOptionsSingleton.GetInstance().GetBrickColor3Int() == 1)
                this.button8.Enabled = false;
            else if (GameOptionsSingleton.GetInstance().GetBrickColor3Int() == 10)
                this.button3.Enabled = false;
            else {
                this.button8.Enabled = true;
                this.button3.Enabled = true;
            }

            if (GameOptionsSingleton.GetInstance().GetBrickColor2Int() == 1)
                this.button7.Enabled = false;
            else if (GameOptionsSingleton.GetInstance().GetBrickColor2Int() == 10)
                this.button4.Enabled = false;
            else {
                this.button4.Enabled = true;
                this.button7.Enabled = true;
            }

            if (GameOptionsSingleton.GetInstance().GetBrickColor1Int() == 1)
                this.button6.Enabled = false;
            else if (GameOptionsSingleton.GetInstance().GetBrickColor1Int() == 10)
                this.button5.Enabled = false;
            else {
                this.button5.Enabled = true;
                this.button6.Enabled = true;
            }

        }

        private void RedrawSoundInt() {
            this.soundLabel.Text = GameOptionsSingleton.GetInstance().GetSoundSetting().ToString();
            if (GameOptionsSingleton.GetInstance().GetSoundSetting() == 0)
                this.button9.Enabled = false;
            else if (GameOptionsSingleton.GetInstance().GetSoundSetting() == 10)
                this.button10.Enabled = false;
            else {
                this.button9.Enabled = true;
                this.button10.Enabled = true;
            }
        }

        private void Close(object sender, EventArgs e) {
            this.Hide();
        }

        private void SaveMemento(object sender, EventArgs e) {

            _originator.SoundSetting = GameOptionsSingleton.GetInstance().GetSoundSetting();
            _originator.BrickColor1 = GameOptionsSingleton.GetInstance().GetBrickColor1Int();
            _originator.BrickColor2 = GameOptionsSingleton.GetInstance().GetBrickColor2Int();
            _originator.BrickColor3 = GameOptionsSingleton.GetInstance().GetBrickColor3Int();

            _caretaker.Memento = _originator.SaveMemento();

            SaveTextChange();
        }

        private void LoadMemento(object sender, EventArgs e) {

            if(_caretaker.Memento != null) {
                _originator.LoadMemento(_caretaker.Memento);
                GameOptionsSingleton.GetInstance().SetAll(_originator.SoundSetting, _originator.BrickColor1, _originator.BrickColor2, _originator.BrickColor3);

                RedrawPictureBoxes();
                RedrawSoundInt();
                LoadTextChange();
            }
            else {
                LoadTextChangeNothingToLoad();
            }
            
        }


        private async void SaveTextChange() {
            this.button11.Text = "Saved!";
            await Task.Delay(1000);
            this.button11.Text = "Save";
        }

        private async void LoadTextChange() {
            this.button12.Text = "Loaded!";
            await Task.Delay(1000);
            this.button12.Text = "Load";
        }

        private async void LoadTextChangeNothingToLoad() {
            this.button12.Text = "Empty";
            await Task.Delay(1000);
            this.button12.Text = "Load";
        }

    }
}
