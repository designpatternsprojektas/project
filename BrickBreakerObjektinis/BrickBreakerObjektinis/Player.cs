﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    class Player {
        public Rectangle rectPlayer;
        public Rectangle moverPlatform;
        public PictureBox racket;

        public Player(PictureBox ball, PictureBox racket) {
            rectPlayer = new Rectangle(ball.Location.X, ball.Location.Y, ball.Width, ball.Height); 
            this.racket = racket;
            moverPlatform = new Rectangle(racket.Location.X, racket.Location.Y, racket.Width, racket.Height);
            this.racket.Image = TextureHandler.GetInstance().GetImage("platform.png");
            this.racket.SizeMode = PictureBoxSizeMode.StretchImage;
        }
    }
}
