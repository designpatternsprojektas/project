﻿using Microsoft.VisualBasic.PowerPacks;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.builder_power_up;
using BrickBreakerObjektinis.power_up_startegy;

namespace BrickBreakerObjektinis.PowerUps {
    class PowerUp : IPowerUpPlan
    {
        public static int Speed_Dec = 1;
        public static int Speed_Inc = 2;
        public static int Point_Dec = 3;
        public static int Point_Inc = 4;

        public int type;
        protected const int Speed = 7;
        public int x, y;
        public PictureBox img;
        public RectangleShape rectangleShape;
        public Rectangle rectangle;

        protected int bonusToSpeed;
        protected int bonusToScore;
        protected bool copyBallBool;

        public IBehaviour Behaviour;

        private string _path;
        private int _speed;
        private int _points;

        public PowerUp(int type)
        {
            this.type = type;
            bonusToSpeed = 0;
            bonusToScore = 0;
            copyBallBool = true;
        }

        public PowerUp(int x, int y, PictureBox box) {
            bonusToSpeed = 0;
            bonusToScore = 0;
            copyBallBool = true;

            this.x = x;
            this.y = y;
            this.img = box;
            rectangleShape = new RectangleShape(x, y, 45, 30);
            rectangle = new Rectangle(x, y, rectangleShape.Width, rectangleShape.Height);
        }

        public int GetPowerUp(int points) {
            return Behaviour.AbstractBehaviuour(points);
        }

        public bool GetCopyBallBool() {
            return copyBallBool;
        }

        public void DropMe() {
            y += Speed;
            rectangleShape.Top += Speed;
            img.Top += Speed;
          
            rectangle.X = x;
            rectangle.Y = y;
        }

        public void SetImage(string path)
        {
            _path = path;
        }

        public void SetValues(int speed, int points)
        {
            _speed = speed;
            _points = points;
        }


        public void SetXandY(int x, int y, PictureBox box)
        {
            this.x = x;
            this.y = y;

            this.img = box;
            rectangleShape = new RectangleShape(x, y, 45, 30);
            rectangle = new Rectangle(x, y, rectangleShape.Width, rectangleShape.Height);
            rectangleShape.BackgroundImage = TextureHandler.GetInstance().GetImage(_path);
        }

 
    }
}
