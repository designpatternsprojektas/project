﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.Adapter;
using BrickBreakerObjektinis.builder_power_up;

namespace BrickBreakerObjektinis.PowerUps {
    class PowerUp_Factory {

        private LoggerAdapter adapter = new LoggerAdapter(Logger.GetInstance());
        public PowerUp_Factory() {

        }

        public PowerUp CreatePowerUp(int x, int y, PictureBox pic) {

            PowerUp pUp = null;

            Random rnd = new Random();
            int rndInt = rnd.Next(1, 6);

            IPowerUpBuilder builder = null;
            PowerUpEngineer robotEngineer = null;

            switch (rndInt) {
                case 1:
                    adapter.Print("PowerUp", "PowerItemIncSpeedBuilder");
                    builder = new PowerItemIncSpeedBuilder();
                    robotEngineer = new PowerUpEngineer(builder);
                    robotEngineer.MakeItem(x, y, pic);
                    pUp = robotEngineer.GetItem();
                    break;
                case 2:
                    adapter.Print("PowerUp", "PowerItemDecSpeedBuilder");
                    builder = new PowerItemDecSpeedBuilder();
                    robotEngineer = new PowerUpEngineer(builder);
                    robotEngineer.MakeItem(x, y, pic);
                    pUp = robotEngineer.GetItem();
                    break;
                case 3:
                    adapter.Print("PowerUp", "PowerItemIncPointsBuilder");
                    builder = new PowerItemIncPointsBuilder();
                    robotEngineer = new PowerUpEngineer(builder);
                    robotEngineer.MakeItem(x, y, pic);
                    pUp = robotEngineer.GetItem();
                    break;
                case 4:
                    adapter.Print("PowerUp", "PowerItemDecPointsBuilder");
                    builder = new PowerItemDecPointsBuilder();
                    robotEngineer = new PowerUpEngineer(builder);
                    robotEngineer.MakeItem(x, y, pic);
                    pUp = robotEngineer.GetItem();
                    break;
                case 5:
                    pUp = new PowerUp_plusBall(x, y, pic);
                    break;
                default:
                    pUp = null;
                    break;
            }
            return pUp;
        }

    }
}
