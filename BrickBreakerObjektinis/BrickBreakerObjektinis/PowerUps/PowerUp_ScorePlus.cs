﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.power_up_startegy;

namespace BrickBreakerObjektinis.PowerUps {
    class PowerUp_ScorePlus : PowerUp {

        public PowerUp_ScorePlus(int x, int y, PictureBox box) : base(x, y, box) {
            rectangleShape.BackgroundImage = TextureHandler.GetInstance().GetImage("scorePlus.png");

            Random rnd = new Random();
            bonusToScore = rnd.Next(1, 2) * 10;
            Behaviour = new ScoreInc();
        }

        public IBehaviour GetScorePlusBehaviour()
        {
            return Behaviour;
        }

    }
}
