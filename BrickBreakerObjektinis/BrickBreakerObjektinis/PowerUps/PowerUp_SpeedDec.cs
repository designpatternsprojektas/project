﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.power_up_startegy;

namespace BrickBreakerObjektinis.PowerUps {
    class PowerUp_SpeedDec : PowerUp {

        public PowerUp_SpeedDec(int x, int y, PictureBox box) : base(x, y, box) {
            rectangleShape.BackgroundImage = TextureHandler.GetInstance().GetImage("speedDec.png");

            Behaviour = new SpeedDec();
            bonusToSpeed = -1;
        }

        public IBehaviour GetSpeedDecBehaviour()
        {
            return Behaviour;
        }

    }
}
