﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.power_up_startegy;

namespace BrickBreakerObjektinis.PowerUps {

    class PowerUp_SpeedInc : PowerUp {

        public PowerUp_SpeedInc(int x, int y, PictureBox box) : base(x, y, box) {
            rectangleShape.BackgroundImage = TextureHandler.GetInstance().GetImage("speedInc.png");

            Random rnd = new Random();
            bonusToSpeed = rnd.Next(1, 3);
            Behaviour = new SpeedInc();
        }

        public IBehaviour GetSpeedIncBehaviour()
        {
            return Behaviour;
        }
    }
}
