﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.power_up_startegy;

namespace BrickBreakerObjektinis.PowerUps {
    class PowerUp_plusBall : PowerUp {

        public PowerUp_plusBall(int x, int y, PictureBox box) : base(x, y, box) {
            rectangleShape.BackgroundImage = TextureHandler.GetInstance().GetImage("plusBall.png");
            Behaviour = new ScoreInc();
            copyBallBool = true;
        }

        public IBehaviour GetPlusBallBehaviour()
        {
            return Behaviour;
        }
    }
}
