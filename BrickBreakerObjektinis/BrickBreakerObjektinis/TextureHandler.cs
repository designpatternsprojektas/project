﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis {
    class TextureHandler {
        private static TextureHandler TextureInstance;

        private TextureHandler() {
            
        }

        public static TextureHandler GetInstance() {
            if (TextureInstance == null) {
                TextureInstance = new TextureHandler();
            }
            return TextureInstance;
        }

        public Image GetImage(string path) {
    
            return Image.FromFile("../" + path);
        }
    }
}
