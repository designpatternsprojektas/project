﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis.builder_power_up
{
    interface IPowerUpBuilder
    {
        void BuildImage(PictureBox pic);
        void BuildValues();
        PowerUp GetItem();
        void BuildPosition(int x, int y, PictureBox box);
    }
}
