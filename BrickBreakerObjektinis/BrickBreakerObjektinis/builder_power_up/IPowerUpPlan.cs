﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrickBreakerObjektinis.builder_power_up
{
    interface IPowerUpPlan
    {
        void SetImage(string path);
	    void SetValues(int speed, int points);
        void SetXandY(int x, int y, PictureBox box);
    }
}
