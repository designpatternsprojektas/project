﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.power_up_startegy;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis.builder_power_up
{
    class PowerItemDecSpeedBuilder : IPowerUpBuilder
    {
        private PowerUp _item;

        public PowerItemDecSpeedBuilder()
        {
            _item = new PowerUp(PowerUp.Speed_Dec);
            _item.Behaviour = new SpeedDec();
        }

        public void BuildImage(PictureBox pic)
        {
            _item.SetImage("speedDec.png");
        }

        public void BuildValues()
        {
            _item.SetValues(-2, 0);
        }

        public PowerUp GetItem()
        {
            return _item;
        }

        public void BuildPosition(int x, int y, PictureBox box)
        {
            _item.SetXandY(x, y, box);
        }
    }
}
