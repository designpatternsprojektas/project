﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.power_up_startegy;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis.builder_power_up
{
    class PowerItemIncPointsBuilder : IPowerUpBuilder
    {
        private PowerUp _item;

        public PowerItemIncPointsBuilder()
        {
            _item = new PowerUp(PowerUp.Point_Inc);
            _item.Behaviour = new ScoreInc();
        }

        public void BuildImage(PictureBox pic)
        {
            _item.SetImage("scorePlus.png");
        }

        public void BuildValues()
        {
            _item.SetValues(0, 5);
        }

        public PowerUp GetItem()
        {
            return _item;
        }

        public void BuildPosition(int x, int y, PictureBox box)
        {
            _item.SetXandY(x, y, box);
        }
    }
}
