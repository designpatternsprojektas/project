﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis.builder_power_up
{
    class PowerUpEngineer
    {
        private IPowerUpBuilder _builder;

        public PowerUpEngineer(IPowerUpBuilder builder)
        {
            _builder = builder;
        }

        public PowerUp GetItem()
        {
            return _builder.GetItem();
        }

        public void MakeItem(int x, int y, PictureBox pic)
        {
            _builder.BuildImage(pic);
            _builder.BuildValues();
            _builder.BuildPosition(x, y, pic);
        }
    }
}