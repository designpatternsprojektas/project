﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis.chain_of_responsibility
{
    abstract class AbstractDestroyer
    {
        public static int Red = 1;
        public static int Green = 2;
        public static int Blue = 3;
        public static int Black = 4;
        public static List<PowerUp> powerUpItems;

        protected int Level;

        public AbstractDestroyer NextDestroyer;

        public void SetNextDestroyer(AbstractDestroyer a)
        {
            NextDestroyer = a;
        }

        public void DestroyItem(int level)
        {
            if (this.Level <= level)
            {
                Destroy();
            }

            if (NextDestroyer != null)
            {
                NextDestroyer.DestroyItem(level);
            }
        }

         protected abstract void Destroy();
    }
}
