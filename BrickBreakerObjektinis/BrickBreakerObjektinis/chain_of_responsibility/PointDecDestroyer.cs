﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.PowerUps;

namespace BrickBreakerObjektinis.chain_of_responsibility
{
    class PointDecDestroyer : AbstractDestroyer
    {

        public PointDecDestroyer(int lvl)
        {
            this.Level = lvl;
        }
        protected override void Destroy()
        {
            Form1.DeleteSpecificItem(PowerUp.Point_Dec);
        }
    }
}
