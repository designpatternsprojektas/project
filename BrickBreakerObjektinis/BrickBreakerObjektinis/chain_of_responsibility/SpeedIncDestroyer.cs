﻿using BrickBreakerObjektinis.PowerUps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.chain_of_responsibility
{
    class SpeedIncDestroyer : AbstractDestroyer
    {

        public SpeedIncDestroyer(int lvl)
        {
            this.Level = lvl;
        }
        protected override void Destroy()
        {
            Form1.DeleteSpecificItem(PowerUp.Speed_Inc);
        }
    }
}
