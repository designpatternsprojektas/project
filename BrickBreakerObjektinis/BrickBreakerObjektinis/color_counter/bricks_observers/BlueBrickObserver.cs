﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.color_counter.subject;

namespace BrickBreakerObjektinis.bricks_observers
{
    class BlueBrickObserver : IObserver
    {
        private System.Windows.Forms.Label label;
        public BlueBrickObserver(ISubject s, System.Windows.Forms.Label l)
        {
            s.Register(this);
            this.label = l;
        }

        public void update(int red, int green, int blue)
        {
            label.Text = blue + "";
        }
    }
}
