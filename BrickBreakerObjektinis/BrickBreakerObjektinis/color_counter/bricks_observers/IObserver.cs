﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.bricks_observers
{
    public interface IObserver
    {
        void update(int red, int green, int blue);
    }
}
