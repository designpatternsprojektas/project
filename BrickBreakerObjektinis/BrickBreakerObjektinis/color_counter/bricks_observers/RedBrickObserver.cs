﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.color_counter.subject;

namespace BrickBreakerObjektinis.bricks_observers
{
    class RedBrickObserver : IObserver
    {
        private System.Windows.Forms.Label label;
        public RedBrickObserver(ISubject s, System.Windows.Forms.Label l)
        {
            s.Register(this);
            label = l;
        }

        public void update(int red, int green, int blue)
        {
            label.Text = red + "";
        }
    }
}
