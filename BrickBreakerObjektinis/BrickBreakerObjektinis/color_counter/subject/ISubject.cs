﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrickBreakerObjektinis.bricks_observers;

namespace BrickBreakerObjektinis.color_counter.subject
{
    interface ISubject
    {
        void Register(IObserver o);
        void Unregister(IObserver o);
        void NotifyAll(int red, int green, int blue);
    }
}
