﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrickBreakerObjektinis.flyweight
{
    class BmpFactory
    {
        private static Dictionary<Color, Bitmap> BitmapMap = new Dictionary<Color, Bitmap>(); // flyweight

        public static Bitmap GetBitmap(Color gotColor)
        {
            Bitmap bmp = null;
            if (BitmapMap.ContainsKey(gotColor))
            {
                bmp = BitmapMap[gotColor];
            }

            if (bmp == null)
            {
                bmp = new Bitmap(1, 1);
                for (int xx = 0; xx < bmp.Width; xx++)
                {
                    for (int yy = 0; yy < bmp.Height; yy++)
                    {
                        bmp.SetPixel(xx, yy, gotColor);
                    }
                }
                BitmapMap.Add(gotColor, bmp);
            }
            return bmp;
        }
    }
}
